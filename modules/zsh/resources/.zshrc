############## ZSH CONFIGURATIONS ####################################
# Path to oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Name of the theme to load. If set to "random", it will load a random theme each time oh-my-zsh is loaded.
ZSH_THEME="aca"

# Add plugins. Standard plugins can be found in $ZSH/plugins/. Custom plugins may be added to $ZSH_CUSTOM/plugins/
plugins=(
	dnf
	aca-git
	zsh-syntax-highlighting
	zsh-autosuggestions
)

# Load all themes and plugins from oh-my-zsh
source $ZSH/oh-my-zsh.sh


############## COMMON METHODS ######################################
# Clean up docker resources.
function docker_clean() {
    echo "Removing containers : docker rm -f $(docker ps -aq)"
    docker rm -f $(docker ps -aq) &> temp_docker_clean_output.txt
    exit_code=$?
    if [ $exit_code -eq 0 ]; then 
        cat temp_docker_clean_output.txt
    else
        echo -e "\t ✓ Already clean"
    fi

    echo "Removing volumes : docker volume prune --all -y"
    docker volume prune --all -y &> temp_docker_clean_output.txt
    exit_code=$?
    if [ $exit_code -eq 0 ]; then 
        cat temp_docker_clean_output.txt
    else
        echo -e "\t ✓ Already clean"
    fi
    
    rm -f temp_docker_clean_output.txt
}




############## COMMON CONFIGURATIONS ###############################
# Use nano instead of VIM.
export EDITOR='nano'





############## COMMON ALIAS #######################################
alias xx='sudo kill -9'
alias dclean='docker_clean'
alias dstart='sudo systemctl start docker; docker_clean'
alias mciwt='mvn clean install -Dmaven.test.skip=true'
alias mvntree='mvn dependency:tree > dtree.txt; mvn help:effective-pom -Dverbose=true > epom.xml'
alias mvnepom='mvn help:effective-pom -Dverbose=true > epom.xml'
alias dorepo="sudo service docker stop > /dev/null 2>&1 && sudo rm -f /var/lib/docker/network/files/local-kv.db && sudo service docker start && echo Success"
alias zshrc='gedit ~/.zshrc'
alias refresh='source ~/.zshrc'

alias jh='echo $JAVA_HOME'
alias j-update='export PATH=$JAVA_HOME/bin:$PATH'
