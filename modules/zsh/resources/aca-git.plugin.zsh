function git_check_local_dirty() {
    git_local_dirty_icon=᚜
    local_dirty_check=$(git status --porcelain)
    if [ -z "$local_dirty_check" ]; then
        echo "%{$fg_bold[green]%}$git_local_dirty_icon%{$reset_color%}"
    else
        echo "%{$fg_bold[yellow]%}$git_local_dirty_icon%{$reset_color%}"
    fi
  }
  
  function git_get_branch() {
    echo $(git branch --show-current 2>/dev/null)
  }
  
  function git_get_username() {
    echo $(git config user.name)
  }
  
  function git_get_user_email() {
    echo $(git config user.email)
  }
  
  function git_check_upstream() {
    upstream_status=""
    upstream_branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null) 
    
    if [ -z $upstream_branch ]; then
        upstream_status="%{$fg_bold[blue]%}↗↙%{$reset_color%}"
    else 
      
      local_branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
      
      if git log --oneline ${upstream_branch}..${local_branch} | grep -q .; then
        # Local branch has additional commits compared to remote branch
        upstream_status="%{$fg_bold[yellow]%}↗%{$reset_color%}"
      else
        # Local branch has  no additional commits compared to remote branch.
        upstream_status="%{$fg_bold[green]%}↗%{$reset_color%}"
      fi
      
      if git log --oneline ${local_branch}..${upstream_branch} | grep -q .; then # 1 if file is empty. 0 otherwise
        # Remote branch has additional commits compared to local branch
        upstream_status="${upstream_status}%{$fg_bold[yellow]%}↙%{$reset_color%}"
      else
        # Remote branch has no additional commits compared to local branch
        upstream_status="${upstream_status}%{$fg_bold[green]%}↙%{$reset_color%}"
      fi
      
    fi
    
    echo $upstream_status
  }
  
  function git_info() {
    git_prompt=""
    
    is_git_managed=0
    git rev-parse --is-inside-work-tree > /dev/null 2>&1
    exit_code=$?
    if [ $exit_code -eq 0 ]; then
      is_git_managed=1
    fi
    
    if [ $is_git_managed -eq 1 ]; then
      git_prompt="$(git_check_local_dirty) $(git_get_branch) $(git_check_upstream) $(git_get_username) [$(git_get_user_email)]"
    fi
    
    echo $git_prompt
  }
  