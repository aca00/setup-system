# Requires aca-git.plugin.zsh 
PROMPT="
%{$fg_bold[cyan]%}%d%{$reset_color%} "
# Single quote is necessary for loading git_info dynamically every time.
PROMPT+='$(git_info)
'

