#!/bin/bash

# Check if git is installed
git --version 2>&1 >/dev/null # https://stackoverflow.com/a/7292662/12616968
GIT_IS_AVAILABLE=$?
if [ "$GIT_IS_AVAILABLE" -ne 0 ]; then 
  echo "Git is not installed. Set up git first."
  exit 1
fi

ZSHRC_INIT_FLAG=0
if [ -f "$HOME/.zshrc" ]; then
  echo ".zshrc exists. ZSH won't show initial configuration window."
else
  # Create an empty .zshrc file to skip the initial configuration window.
  # This file will be replaced later. 
  ZSHRC_INIT_FLAG=1
  touch "$HOME/.zshrc"
fi


# Install zsh 
echo "Installing zsh"
sudo apt install zsh -y

if [ "$?" -ne 0 ]; then 
  read -p "Installation wasn't successful. Continue if zsh is already installed by another user. Do you want to continue ? (y/n) " CAN_CONTINUE
  if [ "$CAN_CONTINUE" = "n" ]; then 
    echo "Exiting setup"
    exit 2
  fi
fi

# Setting up oh-my-zsh
curl --resolve raw.githubusercontent.com:9700:185.199.108.133 https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -o "$HOME/oh-my-zsh-install.sh"

if [ "$?" -ne 0 ]; then 
  echo "Couldn't download oh-my-zsh installation script. Try again later"
  exit 1
fi

bash "$HOME/oh-my-zsh-install.sh" 2>&1
rm "$HOME/oh-my-zsh-install.sh" # Remove the installation script.

# Setting up syntax-highlighting for zsh. 
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# Setting up auto suggestions. (from history)
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# Update plugin
echo "Updating plugin"
CUSTOM_PLUGIN_DIR="$HOME/.oh-my-zsh/custom/plugins"
mkdir -p $CUSTOM_PLUGIN_DIR
cp "$(pwd)/resources/aca-git.plugin.zsh" $CUSTOM_PLUGIN_DIR


# Update theme
echo "Updating theme"
CUSTOM_THEME_DIR="$HOME/.oh-my-zsh/custom/themes"
mkdir -p $CUSTOM_THEME_DIR
cp "$(pwd)/resources/aca.zsh-theme" $CUSTOM_THEME_DIR


# Update .zshrc 
echo "Creating .zshrc and populating with required information."
if [ -f "$HOME/.zshrc" ] && [ $ZSHRC_INIT_FLAG -eq 0 ]; then
  read -p ".zshrc already exists. Do you want to replace ? (y/n) " CAN_REPLACE
  if [ "$CAN_REPLACE" = "y" ]; then 
    # Replace .zshrc with the default one.
    cp "$(pwd)/resources/.zshrc" "$HOME/.zshrc"
  elif [ "$CAN_REPLACE" = "n" ]; then 
    echo "Skipping"
  else 
    echo "Invalid input. Exiting."
    exit 1
  fi
else
  cp "$(pwd)/resources/.zshrc" "$HOME/.zshrc"
fi

# Finally change the shell
echo "Changing shell"
chsh -s /usr/bin/zsh
echo "Shell changed. Login again."
